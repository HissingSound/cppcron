#include <string>
#include <vector>
#include <map>
#include <ctype.h>
#include <iostream>
#include <time.h>

#include "cron.hpp"

// Helpers
// Minutes
const unsigned MIN_FROM   = 0;
const unsigned MIN_TO     = 59;
// Hours
const unsigned HOUR_FROM  = 0;
const unsigned HOUR_TO    = 23;
// Month days
const unsigned MDAY_FROM  = 1;
const unsigned MDAY_TO    = 31;
// Months
const unsigned MONTH_FROM = 1;
const unsigned MONTH_TO   = 12;
// Week days
const unsigned WDAY_FROM  = 0;
const unsigned WDAY_TO    = 6;
// Months
const std::map<const std::string, 
               const std::string> MONTHS = {
                                            { "JAN", "1" },
                                            { "FEB", "2" },
                                            { "MAR", "3" },
                                            { "APR", "4" },
                                            { "MAY", "5" },
                                            { "JUN", "6" },
                                            { "JUL", "7" },
                                            { "AUG", "8" },
                                            { "SEP", "9" },
                                            { "OCT", "10" },
                                            { "NOV", "11" },
                                            { "DEC", "12" }
                                          };

// Week days
const std::map<const std::string, 
               const std::string> WEEK_DAYS = {
                                                { "MON", "0" },
                                                { "TUE", "1" },
                                                { "WED", "2" },
                                                { "THU", "3" },
                                                { "FRI", "4" },
                                                { "SAT", "5" },
                                                { "SUN", "6" },
                                              };



bool decode(std::string & chunk, CronPattern::Type type)
{
    if( chunk.empty() )
        return false;
    
    if( type == CronPattern::Type::TP_MONTH )
        for( auto month = MONTHS.begin(); month != MONTHS.end(); month++ )
            for( size_t pos = chunk.find(month->first); pos != std::string::npos; pos = chunk.find(month->first) )
                chunk.replace(pos, month->first.length(), month->second);
    
    if( type == CronPattern::Type::TP_WEEK_DAY )
        for( auto week_day = WEEK_DAYS.begin(); week_day != WEEK_DAYS.end(); week_day++ )
            for( size_t pos = chunk.find(week_day->first); pos != std::string::npos; pos = chunk.find(week_day->first) )
                chunk.replace(pos, week_day->first.length(), week_day->second);
    
    return true;
}

inline bool inrange(unsigned number, unsigned min, unsigned max)
{
    return (min <= number) && ( number <= max );
}

void getrange(const CronPattern::Type & type, unsigned & rangeFrom, unsigned & rangeTo)
{
    switch( type )
    {
        case CronPattern::Type::TP_MIN:
        {
            rangeFrom = MIN_FROM;
            rangeTo   = MIN_TO;
            break;
        }
        
        case CronPattern::Type::TP_HOUR:
        {
            rangeFrom = HOUR_FROM;
            rangeTo   = HOUR_TO;
            break;
        }
        
        case CronPattern::Type::TP_MONTH_DAY:
        {
            rangeFrom = MDAY_FROM;
            rangeTo   = MDAY_TO;
            break;
        }
        
        case CronPattern::Type::TP_MONTH:
        {
            rangeFrom = MONTH_FROM;
            rangeTo   = MONTH_TO;
            break;
        }
        
        case CronPattern::Type::TP_WEEK_DAY:
        {
            rangeFrom = WDAY_FROM;
            rangeTo   = WDAY_TO;
            break;
        }
        
        default:
        {
            return;
        }
    }
}

bool rangeCheck(unsigned number, CronPattern::Type type)
{
    bool result = false;
    switch(type)
    {
        case CronPattern::Type::TP_MIN:       result = inrange(number, MIN_FROM, MIN_TO); break;
        case CronPattern::Type::TP_HOUR:      result = inrange(number, HOUR_FROM, HOUR_TO); break;
        case CronPattern::Type::TP_MONTH_DAY: result = inrange(number, MDAY_FROM, MDAY_TO); break;
        case CronPattern::Type::TP_MONTH:     result = inrange(number, MONTH_FROM, MDAY_TO); break;
        case CronPattern::Type::TP_WEEK_DAY:  result = inrange(number, WDAY_FROM, WDAY_TO); break;
        default: result = false; break;
    }
    
    return result;
}

// Helpers

// CronPattern

CronPattern::CronPattern(CronPattern::Type _type)
{
    Clear();
    type = _type;
}

CronPattern::CronPattern(const unsigned & _type)
{
    Clear();
    type = uint2type(_type);
}

void CronPattern::Clear()
{
    type = TP_NONE;
    set.clear();
}

CronPattern & CronPattern::operator =(const CronPattern& from)
{
    if( this == &from )
        return*this;
    
    Clear();
    return Assign(from);
}

CronPattern & CronPattern::Assign(const CronPattern& from) 
{
    if( this == &from )
        return *this;
    
    ASSIGN_FROM(type);
    ASSIGN_FROM(set);
    
    return *this;
}

bool CronPattern::match(unsigned i) const
{
    if( type == TP_NONE )
        return false;
    
    return set.find(i) != set.end();
}

CronPattern::Type CronPattern::uint2type(const unsigned & type) const
{
    if( type > (unsigned)CronPattern::Type::TP_NONE )
        return CronPattern::Type::TP_NONE;
    else
        return (CronPattern::Type)type;
}

unsigned CronPattern::type2uint(const CronPattern::Type & type) const
{
    return (unsigned)type;
}

CronPattern::~CronPattern()
{
    Clear();
}
// CronPattern

// CronRule

CronRule::CronRule()
{
    Clear();
}

CronRule::CronRule(const std::string & a_cronstr) 
{
    Clear();
    cronstr = a_cronstr;
}

CronRule::~CronRule()
{
    Clear();
}

void CronRule::Clear()
{
    cronstr.clear();
    patterns.clear();
}

CronRule & CronRule::operator =(const CronRule& from)
{
    if( this == &from )
        return *this;
    
    return Assign(from);
}

CronRule & CronRule::Assign(const CronRule& from)
{
    if( this == &from )
        return *this;
    
    ASSIGN_FROM(cronstr);
    ASSIGN_FROM(patterns);
    
    return *this;
}

void CronRule::setRule(const std::string & rule)
{
    Clear();
    cronstr = rule;
}

// If current local time matched returns true
bool CronRule::match() 
{
    time_t rawtime;
    struct tm * timeinfo;
    
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    
    return match(*timeinfo);
}

bool CronRule::match(const unsigned min, const unsigned hour, const unsigned mday, const unsigned mon, const unsigned wday) const
{
    if( patterns.size() < CronPattern::Type::TP_NONE )
        return false;
    
    CronPattern minutes = patterns.at(CronPattern::Type::TP_MIN);
    if( !minutes.match(min) )
        return false;
    
    CronPattern hours = patterns.at(CronPattern::Type::TP_HOUR);
    if( !hours.match(hour) )
        return false;
    
    CronPattern month_days = patterns.at(CronPattern::Type::TP_MONTH_DAY);
    if( !month_days.match(mday) )
        return false;
    
    CronPattern months = patterns.at(CronPattern::Type::TP_MONTH);
    if( !months.match(mon) )
        return false;
    
    CronPattern week_days  = patterns.at(CronPattern::Type::TP_WEEK_DAY);
    if( !week_days.match(wday) )
        return false;
    
    return true;
}

bool CronRule::match(const tm & time) const
{
    if( patterns.size() < CronPattern::Type::TP_NONE )
        return false;
    
    return match(time.tm_min, time.tm_hour, time.tm_mday, time.tm_mon + 1, time.tm_wday);
}

bool CronRule::parse()
{
    if( cronstr.empty() )
        return false;
    
    unsigned type = 0;
    size_t prev(0), pos(0);
    do
    {
        pos = cronstr.find(' ', pos + 1);
        
        std::string chunk;
        if( pos != std::string::npos )
            chunk = cronstr.substr(prev, pos - prev);
        else
            chunk = cronstr.substr(prev);

        CronPattern temp(type);

        if( !decode(chunk, temp.type) )
            return false;

        if( !parse_chunk(chunk, temp) )
            return false;
        
        if( temp.set.size() == 0 )
            return false;
        
        patterns.push_back(temp);
        prev = pos + 1;
        type++;
    } while( pos != std::string::npos);
    
    if( patterns.size() != 5 )
        return false;
    
    return true;
}

bool CronRule::read_number(const std::string & str, size_t & pos, unsigned & number, char & symbol)
{
    std::string temp;
    for(; pos < str.length(); pos++)
    {
        char c = str.at(pos);
        if( isdigit(c) )
            temp.push_back(c);
        else
        {
            // All numbers
            if( c == '*' )
            {
                symbol = c;
                pos++;
                return true;
            }
            
            if( !temp.empty() ) // example "78,"
                number = std::stoi(temp);
            else
                continue; // example ",76"
            
            symbol = c;
            pos++;
            return true;
        }
    }
    
    if( temp.empty() )
        return false;
    
    number = std::stoi(temp);
    symbol = ',';
    
    return true;
}

bool CronRule::parse_chunk(const std::string & chunk, CronPattern & pattern)
{
    if( chunk.empty() )
        return false;

    size_t pos = 0;
    unsigned rangeFrom(0), rangeTo(0);
    bool rangeOn = false;
    unsigned number = 0;
    char c;
    while( read_number(chunk, pos, number, c) )
    {        
        if( c == '*' )
        {
            getrange(pattern.type, rangeFrom, rangeTo);
            for( unsigned i = rangeFrom; i < rangeTo + 1; i++ )
                pattern.set.insert(i);
            break;
        }
        
        if( !rangeCheck(number, pattern.type) )
            return false;
        
        if( c == ',' )
        {
            if( rangeOn )
            {
                rangeTo = number;
                for (unsigned i = rangeFrom; i < rangeTo + 1; i++)
                    pattern.set.insert(i);
                rangeOn = false;
            }
            else
                pattern.set.insert(number);
            
            continue;
        }
        
        if( c == '-' )
        {
            rangeOn = true;
            rangeFrom = number;
        }
    }
    
    return true;
}
// CronRule