#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include "cron.hpp"

using namespace std;

std::vector<std::string> testRules = {
    "",
    "* ",
    "* -",
    "*, * * * *",
    "* * * * *", // Every minute
    "0, * * * *", // One per Hour Every day
    "0-10, * * * *", // Every hour at *:00, *:01, .. *:10
    "59 * * * *",
    "30 2 * * *", // Every day at 2:30
    "Never-give,you up never,let you down", // Wrong format
    "* * * * * 10-20", // Every minute
    "* * * * * * * *", // Every minute
    "10-6, 2 * * *", // Wrong format
    "0- * * * *", // Wrong format
    "70 * * * *",
    "* * * JAN-MAR MON-SUN",
    "* * * JAN-MAR SUN-MON",
    "-* * * * *"
};

tm makeTime(int min = 0, int hour = 0, int mday = 1, int wday = 0, int mon = 0)
{
    tm temp;
    temp.tm_min  = min;
    temp.tm_hour = hour;
    temp.tm_mday = mday;
    temp.tm_wday = wday;
    temp.tm_mon  = mon;
    
    return temp;
}

int main()
{    
    tm time = makeTime(0, 2);
    for(auto it = testRules.begin(); it != testRules.end(); it++)
    {
        CronRule rule(*it);
        if( !rule.parse() )
        {
            std::cout << "Parse \"" << *it << "\"" << "FAILED" << std::endl;
            continue;
        }
        
        if( !rule.match(time) )
        {
            std::cout << "\'Min: "     << time.tm_min;
            std::cout << " Hour: "     << time.tm_hour;  
            std::cout << " MonthDay: " << time.tm_mday;  
            std::cout << " WeekDay: "  << time.tm_wday;  
            std::cout << " Month: "    << time.tm_mon;
            std::cout << "' didn't match by \"" << *it << "\"" << std::endl;
        }
        else
        {
            std::cout << "\'Min: "     << time.tm_min;
            std::cout << " Hour: "     << time.tm_hour;  
            std::cout << " MonthDay: " << time.tm_mday;  
            std::cout << " WeekDay: "  << time.tm_wday;  
            std::cout << " Month: "    << time.tm_mon;
            std::cout << "' matched by \"" << *it << "\"" << std::endl;
        }
    }
    
    return 0;
}