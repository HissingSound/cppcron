#ifndef CRON_HPP
#define CRON_HPP
#include <time.h>
#include <vector>
#include <set>

#ifndef ASSIGN_FROM
#define ASSIGN_FROM(field) this->field = from.field
#endif // ASSIGN_FROM

struct CronPattern 
{ // CronPattern
    typedef std::set<unsigned> TSet;

    enum Type 
    { // Pattern type
        TP_MIN,       // 0-59
        TP_HOUR,      // 0-23
        TP_MONTH_DAY, // 1-31
        TP_MONTH,     // 1-12
        TP_WEEK_DAY,  // 0-6
        TP_NONE       // Default
    };// Pattern type

    CronPattern::Type type;
    CronPattern::TSet set;

    explicit CronPattern(CronPattern::Type _type = CronPattern::Type::TP_NONE);
    explicit CronPattern(const unsigned & type);
    ~CronPattern();

    void Clear();
    CronPattern & Assign(const CronPattern & from);
    CronPattern & operator=(const CronPattern & from);

    bool match(unsigned i) const;
    
    unsigned type2uint(const CronPattern::Type & type) const;
    CronPattern::Type uint2type(const unsigned & type) const;
private:
    CronPattern();
}; // CronPattern

class CronRule 
{ // CronRule
public:
    typedef std::vector<CronPattern> TPatternCollection;
    
    CronRule();
    CronRule(const std::string & cronstr);
    ~CronRule();

    bool parse();

    void Clear();
    CronRule & Assign(const CronRule & from);
    CronRule & operator=(const CronRule & from);

    void setRule(const std::string & rule);
    
    bool match();
    bool match(const unsigned min, const unsigned hour, const unsigned mday, const unsigned mon, const unsigned wday) const;
    bool match(const tm & time) const;

private:
    TPatternCollection patterns;
    std::string cronstr;
    bool parse_chunk(const std::string & chunk, CronPattern & pattern);
    bool read_number(const std::string & str, size_t & pos, unsigned & number, char & symbol);
};// CronRule

#endif /* CRON_HPP */

